from utils import *
from attack import *

class AdvTrainer():
    def __init__(self, model, optimizer, criterion, dae_criterion,
                 attack_method, attack_args,trn_loader, tst_loader,
                 rec_weight = 1, adv_weight = 1, adv_rec_weight = 1,
                 knot_optimizer = None, one_hot=False, use_cuda=False, 
                 train_strategy = 'joint', noise_std = [1.3546*1e-02, 0.2508], noise_num = 1,
                 print_every=None, trainpoint_loadpath = 'checkpoint/default_model.tar',
                 trainpoint_savepath = 'checkpoint/default_model.tar', train_hot = False):
        self.model = model
        self.criterion = criterion
        self.dae_criterion = dae_criterion
        self.optimizer = optimizer
        self.attack_method = attack_method
        self.attack_args = attack_args
        self.trn_loader = trn_loader
        self.tst_loader = tst_loader

        self.rec_weight = rec_weight
        self.adv_weight = adv_weight
        self.adv_rec_weight = adv_rec_weight
        self.knot_optimizer = knot_optimizer
        self.one_hot = one_hot
        # self.use_reconstructions = use_reconstructions
        self.use_cuda = use_cuda
        self.noise_std = noise_std
        self.noise_num = noise_num
        self.trainpoint_loadpath = trainpoint_loadpath
        self.trainpoint_savepath = trainpoint_savepath
        self.train_hot = train_hot
        self.train_strategy = train_strategy

        self.metrics = {
            'loss': {
                'trn':[],
                'tst':[]
            },
            'accuracy': {
                'trn':[],
                'tst':[]
            },
        }
        self.print_every = print_every

    def run(self, epochs):
        if os.path.isfile(self.trainpoint_loadpath):
            self.load_checkpoint(filename=self.trainpoint_loadpath)
            tst_loss, tst_acc = self.test()
            print('Pretrained checkpoint from '+ self.trainpoint_loadpath +' loaded, TstLoss: %.3f, TstAcc: %.3f'
                % (tst_loss, tst_acc))
            self.metrics['accuracy']['tst'].append(tst_acc)
            self.metrics['loss']['tst'].append(tst_loss)
        if (not os.path.isfile(self.trainpoint_loadpath)) or self.train_hot:
            print('[*] Training for %d epochs' % epochs)
            for epoch in range(1, epochs+1):
                trn_loss, trn_acc = self.train()
                tst_loss, tst_acc = self.test()
                if True:#epoch == epochs or epoch == 1:
                    print('[*] Epoch %d, TrnLoss: %.3f, TrnAcc: %.3f, TstLoss: %.3f, TstAcc: %.3f'
                        % (epoch, trn_loss, trn_acc, tst_loss, tst_acc))
                self.metrics['accuracy']['trn'].append(trn_acc)
                self.metrics['accuracy']['tst'].append(tst_acc)
                self.metrics['loss']['trn'].append(trn_loss)
                self.metrics['loss']['tst'].append(tst_loss)
            if self.trainpoint_savepath != '':
                self.save_checkpoint(filename=self.trainpoint_savepath)

    def train(self):
        # notice that this has any effect only on certain modules like Dropout or BatchNorm
        self.model.train()
        adversary = Adversary(self.model, self.attack_method, **self.attack_args)
        if self.train_strategy == 'joint':
            return self.run_epoch(self.trn_loader, adversary)
        elif self.train_strategy == 'alt':
            return self.run_epoch_alt(self.trn_loader, adversary)

    def test(self):
        self.model.eval()
        return self.run_epoch(self.tst_loader, train=False)

    def make_var(self, tensor):
        if self.use_cuda:
            tensor = tensor.cuda()
        return Variable(tensor)

    def run_epoch(self, loader, train=True, adversary = None):
        """
        TODO try to adjust the training ratio of core-CNN VS knots
        TODO + #(default 1) of randn for each feature f_j(x_i)
        """
        n_batches = len(loader)
        cum_loss = 0
        cum_acc = 0
        for i, (X, y) in enumerate(loader):
            X_var = self.make_var(X)
            y_var = self.make_var(one_hotify(y) if self.one_hot else y)

            scores, features = self.model.logits_and_feature(X_var)
            loss_var = self.criterion(scores, y_var)

            if train:
                #train jointly
                #wgt2 = self.model.fc_knot[0].encode.weight[0].clone()
                rec_loss = 0
                for fi, feat_in in enumerate(features):
                    if self.noise_num > 1:
                        feat_in = feat_in.repeat([self.noise_num] + [1]*(len(feat_in.shape)-1))
                    noise = self.noise_std[fi]*torch.randn_like(feat_in)
                    noised_feat_in = feat_in + noise
                    dn_feat_noised = self.model.knots[fi](noised_feat_in)
                    rec_loss += self.rec_weight*self.dae_criterion(dn_feat_noised, feat_in.detach())
                if self.print_every is not None and i % self.print_every == 0:
                    print('[*] Batch %d, clf loss: %.3f, rec loss: %.3e' % (i, loss_var, rec_loss))
                self.optimizer.zero_grad()
                loss_var += rec_loss
                loss_var.backward()
                self.optimizer.step()

            pred = get_argmax(scores)
            acc = get_accuracy(pred, y)
            loss = loss_var.item()
            cum_acc += acc
            cum_loss += loss

            if self.print_every is not None and i % self.print_every == 0:
                print('[*] Batch %d, Loss: %.3f, Acc: %.3f' % (i, loss, acc))

        return cum_loss / n_batches, cum_acc / n_batches

    def run_epoch_alt(self, loader, train=True, adversary = None):
        """
        TODO try to adjust the training ratio of core-CNN VS knots
        TODO + #(default 1) of randn for each feature f_j(x_i)
        """
        n_batches = len(loader)
        cum_loss = 0
        cum_acc = 0
        for i, (X, y) in enumerate(loader):
            X_var = self.make_var(X)
            y_var = self.make_var(one_hotify(y) if self.one_hot else y)

            scores, features = self.model.logits_and_feature(X_var)
            loss_var = self.criterion(scores, y_var)

            if train:
                #train core-CNN
                # if i%2 == 0:
                self.optimizer.zero_grad()
                loss_var.backward()
                self.optimizer.step()
                #train knots
                # X_var, y_var = remove_misclassified(X_var, y_var, model, ret_var = True)
                # wgt1 = self.model.conv1.weight[0].clone()
                # wgt2 = self.model.fc_knot[0].encode.weight[0].clone()
                # self.model.knot_train_mode()
                # self.model.nonknot_eval_mode()

                # else:
                rec_loss = 0
                for fi,feat_in in enumerate(features):
                    if self.noise_num > 1:
                        feat_in = feat_in.repeat(self.noise_num,1,1,1)
                    noise = self.noise_std[fi]*torch.randn_like(feat_in)
                    noised_feat_in = feat_in + noise
                    dn_feat_noised = self.model.knots[fi](noised_feat_in.detach())
                    rec_loss += self.rec_weight*self.dae_criterion(dn_feat_noised, feat_in.detach())
                self.knot_optimizer.zero_grad()
                rec_loss.backward()
                self.knot_optimizer.step()
                if self.print_every is not None and i % self.print_every == 0:
                    print('[*] Batch %d, clf loss: %.3f, rec loss: %.3e' % (i, loss_var, rec_loss)) 

            pred = get_argmax(scores)
            acc = get_accuracy(pred, y)
            loss = loss_var.item()
            cum_acc += acc
            cum_loss += loss

            if self.print_every is not None and i % self.print_every == 0:
                print('[*] Batch %d, Loss: %.3f, Acc: %.3f' % (i, loss, acc))
        
        return cum_loss / n_batches, cum_acc / n_batches

    def save_checkpoint(self, filename='checkpoint.pth.tar'):
        state = {
            'model': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict()}
        torch.save(state, filename)

    def load_checkpoint(self, filename='checkpoint.pth.tar'):
        if os.path.isfile(filename):
            state = torch.load(filename)
            self.model.load_state_dict(state['model'])
            self.optimizer.load_state_dict(state['optimizer'])
        else:
            print('%s not found.' % filename)
