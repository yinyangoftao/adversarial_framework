import numpy as np

import torch
from torch.utils.data import DataLoader

import torchvision
from torchvision import transforms as T
from torchvision import datasets as dset

class halfLeNormalize(object):
    """Normalize to -1..1 in Google Inception style
    """
    def __call__(self, tensor):
        for t in tensor:
            t.sub_(0.5) #.mul_(2.0)
        return tensor

MNIST_TRN_TRANSFORM = T.Compose([
    T.ToTensor(),
    halfLeNormalize(),
])
MNIST_TST_TRANSFORM = T.Compose([
    T.ToTensor(),
    halfLeNormalize(),
])
CIFAR10_TRN_TRANSFORM = T.Compose([
    T.RandomCrop(28),
    T.ToTensor(),
    halfLeNormalize(),
])
CIFAR10_TST_TRANSFORM = T.Compose([
    T.CenterCrop(28),
    T.ToTensor(),
    halfLeNormalize(),                 
])
CIFAR10_TRN_RESNET_TRANSFORM = T.Compose([
    T.RandomCrop(32, padding=4),
    T.RandomHorizontalFlip(),
    T.ToTensor(),
    # T.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    halfLeNormalize()
])

CIFAR10_TST_RESNET_TRANSFORM = T.Compose([
    T.ToTensor(),
    # T.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    halfLeNormalize()
])

CIFAR10_CLASSES = (
    'airplane', 'automobile', 'bird', 'cat', 'deer',
    'dog', 'frog', 'horse', 'ship', 'truck')

def get_mnist_dataset(trn_size=60000, tst_size=10000):
    trainset = dset.MNIST(root='~/data/mnist', train=True,
                          download=True, transform=MNIST_TRN_TRANSFORM)
    trainset.train_data = trainset.train_data[:trn_size]
    trainset.train_labels = trainset.train_labels[:trn_size]
    testset = dset.MNIST(root='~/data/mnist', train=False,
                         download=True, transform=MNIST_TST_TRANSFORM)
    testset.test_data = testset.test_data[:tst_size]
    testset.test_labels = testset.test_labels[:tst_size]
    return trainset, testset

def get_cifar10_dataset(net_type, trn_size=60000, tst_size=10000):
    if net_type == 'kriz':
        train_transform = CIFAR10_TRN_TRANSFORM
        test_transform = CIFAR10_TST_TRANSFORM
    elif net_type == 'resnet':
        train_transform = CIFAR10_TRN_RESNET_TRANSFORM
        test_transform = CIFAR10_TST_RESNET_TRANSFORM
    trainset = dset.CIFAR10(root='~/data/cifar_10', train=True,
                          download=True, transform=train_transform)
    trainset.train_data = trainset.train_data[:trn_size]
    trainset.train_labels = trainset.train_labels[:trn_size]
    testset = dset.CIFAR10(root='~/data/cifar_10', train=False,
                         download=True, transform=test_transform)
    testset.test_data = testset.test_data[:tst_size]
    testset.test_labels = testset.test_labels[:tst_size]
    return trainset, testset

def get_data_loader(trainset, testset, batch_size=128):
    trainloader = DataLoader(trainset, batch_size=batch_size,
                             shuffle=True)
    testloader = DataLoader(testset, batch_size=batch_size,
                            shuffle=False)
    return trainloader, testloader

def build_data(dataset, net_type = ''):
    if dataset == 'mnist':
        return get_mnist_dataset()
    elif dataset == 'cifar':
        return get_cifar10_dataset(net_type)
    else:
        raise Exception('[!] Unsupported dataset')