import os
import sys
import torch
from torch import nn
from torch.autograd import Variable
from utils import *

class FGS_attacker:

    def __init__(self, model, targeted=False, criterion = nn.CrossEntropyLoss(), epsilon0 = 0.1, steps=10, thresh = 1e-3, clip_max = 0.5, clip_min = -0.5, one_hot = False):
        """
        model: the model we want to attack
        criterion: used during training phase of the target network
        """
        self.model = model
        self.targeted = targeted
        self.criterion = criterion
        self.epsilon0 = epsilon0
        self.steps = steps
        self.thresh = thresh
        self.clip_max = clip_max
        self.clip_min = clip_min
        self.one_hot = one_hot

    def attack(self, img_var, target):
        """
        return the perturbation
        currently process one by one
        img_var: original image, guaranteed to have correct prediction
        target: target label if self.targeted == true, otherwise ground truth label
        """
        # targeted attack
        # import ipdb;ipdb.set_trace()
        output = self.model(img_var)
        # if hasattr(self.model, 'output_idx'):
        #     output = output[self.model.output_idx]        
        output = output.squeeze()
        # pred = get_argmax(output,dim = 1)
        if len(output.shape) == 1:
            output = output.unsqueeze(0)
            target = torch.LongTensor([target])
        if self.one_hot:
            loss = self.criterion(output, trycuda(one_hotify(target)))
        else:
            loss = self.criterion(output, trycuda(target))
        loss.backward()
        x_grad = torch.sign(img_var.grad.data)
        if self.targeted:
            x_grad = -x_grad
        # search for epsilon
        nimgs = []
        # import ipdb;ipdb.set_trace()
        for i in range(img_var.shape[0]):
            lo = 0
            hi = self.epsilon0
            while True:
                epsilons = torch.linspace(lo, hi, self.steps)
                epscalars = epsilons.unsqueeze(1).unsqueeze(1).unsqueeze(1).cuda()
                newx = img_var[i].data + epscalars * x_grad[i]
                newx = Variable(newx.clamp(self.clip_min, self.clip_max))
                outputs = self.model(newx)
                preds = get_argmax(outputs)
                #groud_truth_i = get_argmax(target[i].unsqueeze(0)) if self.one_hot else target[i]
                if self.targeted:
                    # TODO targeted not tested
                    lo_idx = torch.nonzero(preds != target[i]).max()
                else:
                    # index of the biggest noise that still correct
                    try:
                        lo_idx = torch.nonzero(preds == target[i]).max()
                    except:
                        print(preds)
                        nimg = newx[0].clone()
                        nimgs.append(nimg.unsqueeze(0))
                        break
                        import ipdb;ipdb.set_trace()
                # if the biggest noise tested still not successful, abandon this attack and return the original image
                if lo_idx == self.steps - 1:
                    hi = 10 * hi
                    nimg = img_var[i].clone()
                    nimgs.append(nimg.unsqueeze(0))
                    break
                if hi - lo <= self.thresh*self.steps:
                    nimg = newx[lo_idx + 1]
                    nimgs.append(nimg.unsqueeze(0))
                    break
                lo = epsilons[lo_idx]
                hi = epsilons[lo_idx + 1]
        # import ipdb;ipdb.set_trace()
        return torch.cat(nimgs)