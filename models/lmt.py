# why use this piece of code as illustration : this piece of code, especially the backward hook and backward in lmt_fc_closure part shows my ability to implement complex algorithms using low-level tools
import torch
import torch.nn.functional as F
import torch.nn as nn
import numpy as np

DEBUG = False
DEVICE = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
BN_PERTURB_SZ = 1e-2
# 1.try deregister hooks
# 2.add train_forward or modify forward func
def dprint(*any):
    if DEBUG:
        print(*any)

def u_vec_hook(module, grad_input, grad_output):
    """
    grad_input for conv is of length 3, seems to correspond to grad to x_in, weight, bias,
    grad_output is length 1, seems to correspond to grad to x_out
    """
    # import ipdb;ipdb.set_trace()
    grad_np = module.u.grad.cpu().numpy()
    dprint(grad_np.shape, grad_np.max(),grad_np.min())
    module.u.data = module.u.grad
    module.u.grad = None

def to_tuple(shape):
    if isinstance(shape, tuple):
        assert len(shape) == 2 and "wrong dimensional maxpooling kernel"
        return shape
    else:
        assert type(shape) == int and "wrong dimensional maxpooling kernel"
        return shape, shape

def factor(imshape, ksize, kstride, pad):
    # TEST the factor function!
    # for avg pooling layer
    imh = imshape[2]
    imw = imshape[3]
    ksize = to_tuple(ksize)
    kstride = to_tuple(kstride)
    pad = to_tuple(pad)
    l = np.ceil(min(ksize[0], imshape[2] + pad[0] * 2 - ksize[0] + 1) / kstride[0])
    l *= np.ceil(min(ksize[1], imshape[3] + pad[1] * 2 - ksize[1] + 1) / kstride[1])
    return np.sqrt(l)

class ConvLips(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3,
                  stride=1, padding=1):
        super().__init__()
        self.layer = nn.Conv2d(in_channels, out_channels, kernel_size,
                  stride, padding, bias=False)
        self.register_parameter('u', None)
        self.handle = self.register_backward_hook(u_vec_hook)

    def reset_parameters(self, input):
        if DEBUG:
            np.random.seed(1)
            shape = (1,)+tuple(input.shape)
            val = np.random.randn(*shape)
            val = np.array(val, dtype =np.float32)
            self.u = nn.Parameter(torch.tensor(val).to(DEVICE))
        else:
            torch.manual_seed(1)
            self.u = nn.Parameter(input.new(input.size()).unsqueeze(0).normal_(0, 1))

    def forward(self, x, l):
        if self.u is None:
            self.reset_parameters(x[0])
        out = self.layer(x)
        self.u.data /= self.u.data.norm()
        u = self.layer(self.u)
        l *= u.norm()
        return out, l

class BNLips(nn.Module):
    def __init__(self, out_channels):
        super().__init__()
        self.out_channels = out_channels
        self.layer = nn.BatchNorm2d(out_channels,eps=2e-5)
        self.register_parameter('u', None)
        self.handle = self.register_backward_hook(u_vec_hook)

    def reset_parameters(self):        
        if DEBUG:
            np.random.seed(1)
            val = np.random.randn(self.out_channels)
            val = np.array(val, dtype =np.float32)
            self.u = nn.Parameter(torch.tensor(val).to(DEVICE))
        else:
            torch.manual_seed(1)
            self.u = nn.Parameter(torch.randn(self.out_channels).to(DEVICE))

    def forward(self, x, l):
        if self.u is None:
            self.reset_parameters()
        if self.training:
            #mean across dim 0,2,3
            mean = x.mean(3).mean(2).mean(0)
            x_meaned = x - mean.unsqueeze(0).unsqueeze(2).unsqueeze(3)
            var = x_meaned ** 2
            var = var.mean(3).mean(2).mean(0)
        else:
            var = self.layer.running_var
        out = self.layer(x)
        # normalize
        self.u.data /= self.u.data.norm()
        # perturb to prevent converge, see lmt paper
        ttsz = torch.tensor(self.u.shape).prod()
        self.u.data += (BN_PERTURB_SZ / ttsz.item()) * torch.randn(self.u.shape, device = DEVICE)
        self.u.data /= (1+BN_PERTURB_SZ)
        z = self.layer.weight / torch.sqrt(var + self.layer.eps)
        u = self.u * z
        l *= u.norm()
        return out, l

class LinearLips(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.layer = nn.Linear(in_channels, out_channels, bias = False)
        self.register_parameter('u', None)
        self.handle = self.register_backward_hook(u_vec_hook)

    def reset_parameters(self, input):
        if DEBUG:
            np.random.seed(1)
            shape = (1,)+tuple(input.shape)
            val = np.random.randn(*shape)
            val = np.array(val, dtype =np.float32)
            self.u = nn.Parameter(torch.tensor(val).to(DEVICE))
        else:
            torch.manual_seed(1)
            self.u = nn.Parameter(input.new(input.size()).unsqueeze(0).normal_(0, 1))

    def forward(self, x, l):
        if self.u is None:
            self.reset_parameters(x[0])
        out = self.layer(x)
        self.u.data /= self.u.data.norm()
        u = self.layer(self.u)
        l *= u.norm()
        return out, l

def lmt_fc_closure(t):
    """
    unlike chainer, pytorch autograd doesn't support function instance, so I use closure
    here instead
    """
    class LmtFc(torch.autograd.Function):
        """
        used for last linear layer
        """
        def __init__(self):
            super().__init__()

        @staticmethod
        def forward(self, x, W, l):
            """
            In the forward pass we receive a Tensor containing the input and return
            a Tensor containing the output. ctx is a context object that can be used
            to stash information for backward computation. You can cache arbitrary
            objects for use in the backward pass using the ctx.save_for_backward method.
            """
            self.t = t
            self.save_for_backward(x, W, l)
            diff = W.unsqueeze(0) - W[t].unsqueeze(1)
            w_norm = diff.norm(dim=2)
            self.diff = diff / (w_norm.unsqueeze(-1) + 1e-22)
            self.w_norm = w_norm
            l = l*w_norm
            # process x
            margin = x[range(len(t)),t].unsqueeze(1) - x
            factor = margin / (l+1e-20)
            factor = factor.clamp(0,1).detach()
            factor[range(len(t)),t] = 2
            # import ipdb; ipdb.set_trace()
            self.factor = factor.min(dim = 1, keepdim=True)[0]
            x += self.factor * l
            return x, l

        @staticmethod
        def backward(self, grad_y, grad_l):
            """
            In the backward pass we receive a Tensor containing the gradient of the loss
            with respect to the output, and we need to compute the gradient of the loss
            with respect to the input.
            """
            # import ipdb;ipdb.set_trace()
            x, W, l = self.saved_tensors
            t = self.t
            if grad_l.max() > 0:
                print(grad_l.max())
            # gy, l, and gy2: all of shape (batch_size, out_dim)
            gy = grad_y.clone()
            gy = gy * self.factor
            gy2 = gy * l
            self.diff *= gy2.unsqueeze(-1)
            # assert self.diff.shape == (x.shape[0], W.shape[0], W.shape[1])
            ret = self.diff
            ret[list(range(len(t))), t] -= self.diff.sum(dim=1)
            # assert ret.shape == (x.shape[0], W.shape[0], W.shape[1])
            ret = ret.sum(dim=0)
            # assert ret.shape == (W.shape[0], W.shape[1])
            return grad_y, ret, (gy * self.w_norm).sum().squeeze()
    return LmtFc

class LastLinerLips(nn.Module):
    def __init__(self, in_channels, out_channels, bias = False):
        super().__init__()
        self.layer = nn.Linear(in_channels, out_channels, bias = bias)
        # self.handle = self.register_backward_hook(u_vec_hook)

    def forward(self,x,l,t=None):
        x = self.layer(x)
        if self.training:
            return lmt_fc_closure(t).apply(x, self.layer.weight, l)
        else:
            return x

class MaxPoolLips(nn.Module):
    def __init__(self, kernel_size=2, stride=2):
        super().__init__()
        self.layer = nn.MaxPool2d(kernel_size, stride)

    def forward(self, x, l):
        out = self.layer(x)
        l *= factor(x.shape,self.layer.kernel_size, self.layer.stride, self.layer.padding)
        return out, l

class AvgPoolLips(nn.Module):
    def __init__(self, kernel_size=2, stride=2):
        super().__init__()
        self.layer = nn.AvgPool2d(kernel_size, stride)

    def forward(self, x, l):
        print("WARNING, AVG not experiment tested!, ported from chainer code")
        out = self.layer(x)
        if len(self.layer.kernel_size) == 1:
          l /= self.layer.kernel_size
        else:
          l /= np.sqrt(self.layer.kernel_size[0]*self.layer.kernel_size[1])
        l *= factor(x.shape,self.layer.kernel_size, self.layer.stride, self.layer.padding)
        return out, l

class LipsCNN(nn.Module):
    def __init__(self, in_shape=(1, 28, 28), n_classes=10, 
        robust_param = 1e-2, lmt_fc = True):
        super().__init__()
        c, w, h = in_shape
        pool_layers = 2
        fc_h = int(h / 2**pool_layers)
        fc_w = int(w / 2**pool_layers)
        self.maxpool = MaxPoolLips(kernel_size=2, stride=2)
        self.conv = ConvLips(c,6)
        self.bn = BNLips(6)
        self.conv2 = ConvLips(6, 16)
        self.bn2 = BNLips(16)
        self.conv3 = ConvLips(16, 32)
        self.bn3 = BNLips(32)
        self.conv4 = ConvLips(32, 64)
        self.bn4 = BNLips(64)
        self.flatten = lambda x: x.view(-1, 64*fc_h*fc_w)
        self.fc1 = LinearLips(64*fc_h*fc_w, 128)
        self.lfc = LastLinerLips(128, n_classes)
        self.robust_param = robust_param
        self.lmt_fc = lmt_fc
        self.epoch_cnt = 0

    def forward(self, x, t = None):
        x, l = self.conv(x, self.robust_param)
        x, l = self.bn(x, l)
        # lipschitz for relu is 1
        x = nn.ReLU()(x)
        # x,l = self.maxpool(x,l)
        x, l = self.conv2(x, l)
        x, l = self.bn2(x, l)
        x = nn.ReLU()(x)
        x,l = self.maxpool(x,l)
        x, l = self.conv3(x, l)
        x, l = self.bn3(x, l)
        x = nn.ReLU()(x)
        x, l = self.conv4(x, l)
        x, l = self.bn4(x, l)
        x = nn.ReLU()(x)
        x,l = self.maxpool(x,l)
        x = self.flatten(x)
        x,l = self.fc1(x,l)
        x = nn.ReLU()(x)
        x = self.lfc(x,l,t)
        return x

    def schedule_robust_param(self,robust_param):
        self.robust_param = robust_param

    def remove_u_vec(self):
        state_dict = self.state_dict()
        all_keys = list(state_dict.keys())
        for ky in all_keys:
            if ky[-2:] == '.u':
                state_dict.pop(ky)#[ky]
        return state_dict


    def init_weight(self):
        if DEBUG:
            # import ipdb;ipdb.set_trace()
            # implement init in chainer toycnn.py and compare
            for m in self.modules():
                if isinstance(m, ConvLips) or isinstance(m, LinearLips) or isinstance(m, LastLinerLips):
                    np.random.seed(1)
                    sigma = np.sqrt(2/np.prod(tuple(m.layer.weight.shape)))
                    val = sigma*np.random.randn(*tuple(m.layer.weight.shape))
                    val = np.array(val, dtype =np.float32)
                    m.layer.weight.data = torch.tensor(val).to(DEVICE)
                elif isinstance(m, BNLips):
                    nn.init.constant_(m.layer.weight, 1)
                    nn.init.constant_(m.layer.bias, 0)
        else:
            for m in self.modules():
                if isinstance(m, ConvLips):
                    nn.init.kaiming_normal_(m.layer.weight)
                elif isinstance(m, BNLips):
                    nn.init.constant_(m.layer.weight, 1)
                    nn.init.constant_(m.layer.bias, 0)
                elif isinstance(m, LinearLips) or isinstance(m, LastLinerLips):
                    nn.init.normal_(m.layer.weight, 0, 1.0/np.sqrt(m.layer.weight.shape[1]))