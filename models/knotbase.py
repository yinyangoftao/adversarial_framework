import torch
from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn


class KnotBase(nn.Module):
    # achieve 98.9% on MNIST
    def __init__(self, data_channel = 1, class_num = 10):
        super().__init__()
        self.conv1 = nn.Conv2d(data_channel, 64, 5)
        self.conv2 = nn.Conv2d(64, 128, 5)
        self.conv3 = nn.Conv2d(128,128, 5,stride=5)
        self.fc1 = nn.Linear(128*4*4, class_num)

    def forward(self, x):
        # import ipdb;ipdb.set_trace()
        x = F.relu(self.conv1(x))    
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = x.view(x.size(0), -1)
        logits = self.fc1(x)
        return logits

class Knot(nn.Module):

    def __init__(self, data_channel = 1, class_num = 10):
        super().__init__()
        self.nonknots = []
        self.knots = []
        self.conv1 = nn.Conv2d(data_channel, 64, 5)
        
        self.conv1_knot = nn.Sequential(
                nn.Conv2d(64, 32, 5),
                nn.LeakyReLU(0.2),
                nn.ConvTranspose2d(32, 64, 5)
            )#nn.Sequential(DAE_Conv(64,32))
        self.knots.append(self.conv1_knot)

        self.conv2 = nn.Conv2d(64, 128, 5)
        self.conv3 = nn.Conv2d(128,128, 5,stride=5)

        # question here: in the fortified network paper, they said they use size 5 kernel for all autoencoders, but it's impossible for this 3rd layer
        self.conv3_knot = nn.Sequential(
                nn.Conv2d(128, 32, 3),
                nn.LeakyReLU(0.2),
                nn.ConvTranspose2d(32, 128, 3)
            )#nn.Sequential(DAE_Conv(64,32))
        self.knots.append(self.conv3_knot)

        self.fc1 = nn.Linear(128*4*4, class_num)
        
        # self.fc_knot = nn.Sequential(
        #         nn.Linear(class_num, 32, 5),
        #         nn.LeakyReLU(0.2),
        #         nn.Linear(32, class_num, 5)
        #     )#nn.Sequential(DAE_Linear(class_num, 64))
        # self.knots.append(self.fc_knot)

        self.nonknots = [self.conv1, self.conv2, self.conv3, self.fc1]

    def forward(self, D):
        # import ipdb;ipdb.set_trace()
        features = []
        x = F.relu(self.conv1(D))
        x = self.conv1_knot(x)
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = self.conv3_knot(x)
        x = x.view(x.size(0), -1)
        x = self.fc1(x)
        # x = self.fc_knot(x)
        return x

    def param_split(self):
        core_params = []
        knot_params = []
        for layer in self.knots:
            knot_params += list(layer.parameters())
        for layer in self.nonknots:
            core_params += list(layer.parameters())
        return core_params, knot_params

    def logits_and_feature(self, D):
        # import ipdb;ipdb.set_trace()
        features = []
        x = F.relu(self.conv1(D))
        x = self.conv1_knot(x)
        features.append(x)
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = self.conv3_knot(x)
        features.append(x)
        x = x.view(x.size(0), -1)
        x = self.fc1(x)
        # x = self.fc_knot(x)
        # features.append(x)
        return x, features

    # def knot_eval_mode(self):
    #     for knot in self.knots:
    #         knot.eval()

    # def knot_train_mode(self):
    #     for knot in self.knots:
    #         knot.train()

    # def nonknot_eval_mode(self):
    #     for knot in self.nonknots:
    #         knot.eval()

    # def nonknot_train_mode(self):
    #     for knot in self.nonknots:
    #         knot.train()

class DAE_Linear(nn.Module):

    def __init__(self, in_channel, ebd_channel):
        super().__init__()
        self.encode = nn.Linear(in_channel, ebd_channel)
        self.decode = nn.Linear(ebd_channel, in_channel)

    def forward(self, x):
        code = self.encode(x)
        x = self.decode(code)
        return x

class DAE_Conv(nn.Module):

    def __init__(self, in_channel, ebd_channel):
        super().__init__()
        #
        self.enconv = nn.Conv2d(in_channel, ebd_channel, 5)
        self.deconv = nn.ConvTranspose2d(ebd_channel, in_channel, 5)

    def forward(self, x):
        code = self.enconv(x)
        x = self.deconv(code)
        return x