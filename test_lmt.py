import torch
import torch.nn as nn
from torch import optim
import torchvision
from torchvision import transforms as T
from torchvision import datasets as dset
from torch.utils.data import DataLoader
from tqdm import tqdm
import os
from models import lmt
from models.lmt import *
from models import lmtbase
from models.lmtbase import *
import argparse
from utils import *
from attack import *

# DEVICE = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

class halfLeNormalize(object):
    """Normalize to -1..1 in Google Inception style
    """
    def __call__(self, tensor):
        for t in tensor:
            t.sub_(0.5) #.mul_(2.0)
        return tensor

MNIST_TRN_TRANSFORM = T.Compose([
    T.ToTensor(),
    halfLeNormalize(),
])

if __name__ == "__main__":
    # import ipdb;ipdb.set_trace()
    parser = argparse.ArgumentParser("lmt")
    parser.add_argument('-cp','--check_point_sav', type=str, default='', help='location of the checkpoint, e.g., save to /mnt/hhd/lmt_reproduce/checkpoint/')
    parser.add_argument('-lr', '--learning_rate', type=float, default=1e-3, help='learning rate')
    parser.add_argument('-cpp','--check_point_pre', type=str, default='', help='location of the pretrained checkpoint')
    parser.add_argument('-ts','--task', type=str, default='eval', help='train?eval?')
    parser.add_argument('-bs', '--batch_size', type=int, default=64, help='batch size')
    parser.add_argument('-ep', '--epoch', type=int, default=10, help='training epochs')
    parser.add_argument('-wm', '--warm_up', type=int, default=5, help='warm up epochs it takes from low robust to end robust parameter')
    parser.add_argument('-atm', '--attack_method', type=str, default='FGS', help='attack batch size')
    parser.add_argument('-atbs', '--attack_batch_size', type=int, default=16, help='attack batch size')
    parser.add_argument('-atbn', '--attack_batch_N', type=int, default=10, help='# of batches of the attack')
    parser.add_argument('-lor', '--low_robust', type=float, default=1e-5, help='low robust parameter')
    parser.add_argument('-eor', '--end_robust', type=float, default=1e-1, help='end robust parameter')

    args = parser.parse_args()

    torch.manual_seed(1)

    print("device:",DEVICE)
    criterion = nn.CrossEntropyLoss()
    model = LipsCNN(robust_param=0).to(DEVICE)
    
    # modv = CNN().to(DEVICE)
    if os.path.isfile(args.check_point_pre):
        print("loading from pretrained")
        state = torch.load(args.check_point_pre)
        model.load_state_dict(state)
    if args.task == 'train':
        # import ipdb;ipdb.set_trace()
        trainset = dset.MNIST(root='~/data/mnist', train=True, transform=MNIST_TRN_TRANSFORM,
        download=True)
        trainloader = DataLoader(trainset, batch_size=args.batch_size,
                                 shuffle=False)
    
        optimizer = optim.Adam(model.parameters(), args.learning_rate)
        model.init_weight()
        model.train()

        # optiv = optim.Adam(modv.parameters(), args.learning_rate)
        # modv.init_weight()
        # modv.train()

        train_loss = 0
        train_crc = 0
        train_ttl = 0
        batch_portion = args.batch_size / len(trainset)
        for epoch in range(args.epoch):
            print("robust param: ",model.robust_param)
            for i,(X,y) in tqdm(enumerate(trainloader)):
                if epoch < args.warm_up:
                    epoch_detail = epoch + i * batch_portion
                    warm_rbp = args.end_robust * (epoch_detail / args.warm_up) + args.low_robust * (1 - epoch_detail / args.warm_up)
                    model.schedule_robust_param(warm_rbp)
                    # if i % 200 == 0:
                        # print("epoch_detail: {}, rbt param: {}".format(epoch_detail,warm_rbp))
                else:
                    model.schedule_robust_param(args.end_robust)

                X = X.to(DEVICE)
                y = y.to(DEVICE)
                logits,l = model(X,y)
                loss_var = criterion(logits, y) #+ model.batch_param_loss()
                optimizer.zero_grad()
                loss_var.backward()
                optimizer.step()

                # logits_v = modv(X)
                # loss_v = criterion(logits_v, y) #+ model.batch_param_loss()
                # optiv.zero_grad()
                # loss_v.backward()
                # optiv.step()

                lbls = logits.max(1)[1]
                train_crc += len(torch.nonzero(lbls == y))
                train_ttl += len(lbls)
                if (i % 100 == 0):
                    print("acc:", train_crc/train_ttl)
                    # break
            print("epoch:",epoch,"l:",l.max())
        
        if args.check_point_sav != '':
            # state = model.state_dict()
            # state.pop('conv.u')
            state2 = model.remove_u_vec()
            torch.save(state2, args.check_point_sav)
            print("save to ", args.check_point_sav)

        testset = dset.MNIST(root='~/data/mnist', train=False, transform=MNIST_TRN_TRANSFORM,
        download=True)
        testloader =  DataLoader(testset, batch_size=args.batch_size,
                                 shuffle=False)
        model.eval()
        test_crc,test_ttl = 0,0
        # import ipdb;ipdb.set_trace()
        for X,y in tqdm(trainloader):
            X = X.to(DEVICE)
            y = y.to(DEVICE)
            logits = model(X)
            lbls = logits.max(1)[1]
            test_crc += len(torch.nonzero(lbls == y))
            test_ttl += len(lbls)
        print("unattacked acc:",test_crc/test_ttl)

    elif args.task == 'eval':
        #check batchnorm parameter setting
        
        # trainset = dset.MNIST(root='~/data/mnist', train=True, transform=MNIST_TRN_TRANSFORM,
        # download=True)
        # trainloader = DataLoader(trainset, batch_size=args.batch_size,
        #                          shuffle=False)
        testset = dset.MNIST(root='~/data/mnist', train=False, transform=MNIST_TRN_TRANSFORM,
        download=True)
        testloader =  DataLoader(testset, batch_size=args.attack_batch_size,
                                 shuffle=False)
        ttl_loss = 0
        print("loading from", args.check_point_pre)
        state = torch.load(args.check_point_pre)
        model.load_state_dict(state)
        # model2 = LipsCNN(robust_param=0).to(DEVICE)
        # model2.load_state_dict(state)
        # model2.eval()
        # model2.schedule_robust_param(0)
        for mod in model.children():
            try:
                mod.handle.remove()
            except:
                print(type(mod))
        model.eval() # training mode would have much higher acc
        model.schedule_robust_param(0)
        
        # test_crc,test_ttl = 0,0
        # for X,y in tqdm(testloader):
        #     X = X.to(DEVICE)
        #     y = y.to(DEVICE)
        #     logits = model(X)
        #     lbls = logits.max(1)[1]
        #     test_crc += len(torch.nonzero(lbls == y))
        #     test_ttl += len(lbls)
        # print("unattacked acc:",test_crc/test_ttl)
        
        attack_batch_size = args.attack_batch_size
        attack_batch_N = args.attack_batch_N
        attack_args_fgs = {'targeted':False, 'criterion':criterion, 'epsilon0': 0.3, 'steps': 10, 'thresh':1e-3}
        attack_args_cw = {'targeted':False, 'confidence': 0.05, 'initial_const':1, 'search_steps': 3, 'max_steps':1000, 'clip_min':-0.5, 'clip_max':0.5}
        attack_args = {"FGS":attack_args_fgs, "CW": attack_args_cw}
        attack_method = args.attack_method
        
        result = measure_robustness(model, testloader, attack_batch_N, attack_method, attack_args[attack_method])
        # import ipdb;ipdb.set_trace()
        print("attack setting:",attack_method, attack_args[attack_method])
        print('lmt : {0} attacked out of {2}, with average norm {1}.'.format(len(result['diff_imgs']), avg_norm(result['diff_imgs']), attack_batch_size*attack_batch_N))
        # advs = result['fool_imgs']
        # origs = result['seed_imgs']
        # print(len(advs))