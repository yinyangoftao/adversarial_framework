"""
	Fortified network inserted some knots between some critical layers of a trained net,
	and they only train those knots.
	Our strategy:
	what if we alternatively train the core-CNN and the DAE part?

"""
import numpy as np
import torch
from torch import optim
import torchvision
import argparse

from datasets import build_data, get_data_loader
from utils import *
from detect_utils import *
from train import *
from attack import *

import sys
from build_model import build_net
from models import *
import copy

"""
May 6th todo: alt: 2 optimizers
need to check noise distribution on each layer
verify effectiveness of adv training
try more knot layers
"""
def inspect_feat_diff(seed_feat, fool_feat):
	diff = []
	for i in range(len(seed_feat)):
		diff += [fool_feat[i] - seed_feat[i]]
	return diff

def train_then_attack(model, loss, attack_method, attack_args, checkpoint_path = '',dataset = 'mnist', net_type = 'caps',
		trpt_fn = 'default.tar', attack_batch_size = 16, attack_batch_N = 10, tr_ep = 2,
		use_recons=True,one_hot=True,res_path = 'result/default'):
	print(attack_args)
	trainset, testset = build_data(dataset, net_type)
	trainloader, testloader = get_data_loader(trainset, testset)
	model_scale = get_model_params_scale(model)
	print("model scale:", model_scale)
	optimizer = optim.Adam(model.parameters(), lr=5e-3)
	# import ipdb;ipdb.set_trace()
	ldpt = '/'.join([checkpoint_path, dataset + net_type, trpt_fn])
	svpt = '/'.join([checkpoint_path, dataset + net_type, trpt_fn])
	trainer = Trainer(model, optimizer, loss,
			trainloader, testloader, use_cuda=True,train_hot = False,
			use_reconstructions=use_recons,one_hot=one_hot,print_every=50,
			trainpoint_loadpath = ldpt, trainpoint_savepath = svpt)
	trainer.run(epochs=tr_ep)
	trainloader, testloader = get_data_loader(trainset, testset, attack_batch_size)
	# import ipdb;ipdb.set_trace()
	result = measure_robustness(model, trainloader, attack_batch_N, attack_method, attack_args)
	print('mean l2 norm of perturbations')
	# import ipdb;ipdb.set_trace()
	print('caps : {0} attacked out of {2}, with average norm {1}.'.format(len(result['diff_imgs']), avg_norm(result['diff_imgs']), attack_batch_size*attack_batch_N))
	advs = result['fool_imgs']
	origs = result['seed_imgs']
	actvs_func = model.output_each_layer
	adv_lids = lid(trainset, advs, actvs_func)
	orig_lids = lid(trainset, origs, actvs_func)
	print("x, relu_conv, p_caps, d_caps, recons_relu_fc1, recons_relu_fc2, recons_img")
	print("origs LID:", orig_lids.mean(dim=0))
	print("advs LID:", adv_lids.mean(dim=0))
	np.save(res_path + 'origs',orig_lids)
	np.save(res_path + 'advs', adv_lids)

if __name__ == "__main__":
	network = CapsuleNetwork(img_colors=1, p_caps=64).cuda()
	decoder = CapsuleDecoder(img_colors=1, reconstruction=True, mask_incorrect=False).cuda()
	# network = CapsuleNetwork(img_colors=3, p_caps=64).cuda()
	# decoder = CapsuleDecoder(img_colors=3, reconstruction=True, mask_incorrect=False).cuda()
	model = CapsuleModel(network, decoder)
	loss = CapsuleLoss(rcsn_scale=0.005)
	# import ipdb;ipdb.set_trace()
	# model = build_net('baby')
	# loss = nn.CrossEntropyLoss()
	attack_args_cw = {'targeted':False, 'confidence': 0.05, 'initial_const':0.01, 'search_steps': 5, 'max_steps':1000, 'clip_min':-0.5, 'clip_max':0.5}
	attack_args_fgs = {'targeted':False, 'criterion':loss, 'epsilon0': 0.2, 'steps': 10, 'thresh':1e-3, 'one_hot':True}
	# train_then_attack(model, loss, 'CW', attack_args_cw, checkpoint_path = 'checkpoint', dataset = 'mnist', net_type = 'baby',trpt_fn = 'halfLeNormalizedModel.tar',
	# 	use_recons=False,one_hot=False,attack_batch_N = 1)

	#train_then_attack(model, loss, 'FGS', attack_args_fgs, checkpoint_path = 'checkpoint', dataset = 'mnist', net_type = 'caps',trpt_fn = 'caps.tar',attack_batch_N = 64,res_path = 'result/FGScaps')
	train_then_attack(model, loss, 'CW', attack_args_cw, checkpoint_path = 'checkpoint', dataset = 'mnist', net_type = 'caps',trpt_fn = 'caps.tar',attack_batch_N = 64,res_path = 'result/CWcaps')
