import sys
sys.path.insert(0, 'models')
from resnet18 import *
from models import *
from models.knotbase import *

def build_net(net_type, in_chnl=1,num_classes=10):
    if net_type == 'baby':
        return BasicNetwork().cuda()
    elif net_type == 'kriz':
        return KrizhevskyNet().cuda()
    elif net_type == 'caps':
        return CapsuleNetwork(img_colors = in_chnl).cuda()
    elif net_type == 'resnet':
        return ResNet18(num_classes).cuda()
    elif net_type == 'knotbase':
        return KnotBase().cuda()
    elif net_type == 'knot':
        return Knot().cuda()
    else:
        raise Exception('[!] Unsupported net')