# Adversarial learning framework

Adversarial learning framework I constructed for research in adversarial attack/defense.

Contains several branches including FGSM/CW_L2 attack, evaluation of CapsNet robustness, and partial pytorch implementation of Lipschitz Margin Training (https://github.com/ytsmiling/lmt)
