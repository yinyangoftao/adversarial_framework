import numpy as np
import torch
from utils import *
from datasets import DataLoader

def lid(dataset, test_data, actvs_func, batch_size = 100, k = 10):
	"""
	based on a random batch sampled from dataset, 
	calculate LID for each item in outputs from each actvs_func 
	applied on data points in test_data. 
	params:
		dataset: where we sample the minibatch from
		test_data: var, the data points that we want to compute LID
		actvs_func: the activation function whose outputs can
		be activations of several layers of a net
		batch_size: size of minibatch
		k: number of nearest we consider to compute LID
	returns:
		lids: of shape (|test_data|,L)
	"""
	if k > batch_size:
		k = batch_size
		print("Warning: k > batch_size! set k to batch_size")
	loader = DataLoader(dataset, batch_size=batch_size,
				shuffle=True)
	batch, labels = next(iter(loader))
	batch = trycuda(batch)
	# N, d = test_data.shape
	actvs_batch = actvs_func(batch)
	L = len(actvs_batch)
	LIDs = []
	#TODO implement batch-wise LID
	for i,x in enumerate(test_data):
		if i % 10 == 0:
			pass
		try:
			with torch.no_grad():
				x = trycuda(x).unsqueeze(0)
				actvs = actvs_func(x)
				#L, S
				# dist2batch = torch.cat(list(map(lambda x, y:l2_norm(x-y).unsqueeze(0), actvs, actvs_batch)),dim=0)
				dist2batch = torch.stack(list(map(lambda x, y:l2_norm(x-y), actvs, actvs_batch)),dim=0)
				knn, knn_idx = dist2batch.topk(k,dim=1,largest=False)
				far = knn[:,-1].unsqueeze(1)
				LID_4_each_layers = -k/((knn/far).log().sum(dim=1))
				LIDs += [LID_4_each_layers]
		except:
			import ipdb;ipdb.set_trace()
	LIDs = torch.stack(LIDs)
	return LIDs