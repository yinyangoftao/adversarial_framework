import sys
sys.path.insert(0, 'attacks')
import torch
from torch import nn
from torch.autograd import Variable
from utils import *
from FGS import FGS_attacker
from cw_l2 import CarliniWagnerL2_attacker

def set_grad(model, cond):
    for p in model.parameters():
        p.requires_grad = cond

class Adversary():
    
    def __init__(self, model, attack_method, **kwargs):
        self.model = model
        if attack_method == 'FGS':
            self.attacker = FGS_attacker(self.model, **kwargs)
        elif attack_method == 'CW':
            self.attacker = CarliniWagnerL2_attacker(self.model, **kwargs)
        else:
            raise Exception('[!] Unknown attack method specified')

    def attack_batch(self, seed_imgs, target_or_grounds):
        set_grad(self.model, False)
        img_var = Variable(seed_imgs.clone(), requires_grad=True)
        
        # if attack == 'FGS':
            
        #     attacker = FGS_attacker(self.model, **kwargs)
        res = self.attacker.attack(img_var, target_or_grounds)
        # else:
        #     raise Exception('[!] Unknown attack method specified')
        
        set_grad(self.model, True)
        if type(res) == tuple:
            fool_image, iters = res
            return fool_image.data.cpu(), iters
        else:
            fool_image = res
            return fool_image.data.cpu()

def keep_successful_attack(seed_img, fool_img, model, targets = None):
    seed_var = Variable(seed_img).cuda()
    fool_var = Variable(fool_img).cuda()
    scores_seed = model(seed_var)
    # if hasattr(model, 'output_idx'):
    #     scores_seed = scores_seed[model.output_idx]
    pred_seed = scores_seed.data.max(1)[1]
    targeted = not (targets is None)
    # if untargeted, since images are filtered, all are correctly predicted,
    # so target (which we want to avoid for untargeted attack) could be set to pred_seed
    if not targeted:
        targets = pred_seed
    scores_fool = model(fool_var)
    # if hasattr(model, 'output_idx'):
    #     scores_fool = scores_fool[model.output_idx]
    pred_fool = scores_fool.data.max(1)[1]
    if targeted:
        success_idx = torch.nonzero(pred_fool == targets)
    else:
        success_idx = torch.nonzero(pred_fool != targets)
    if len(success_idx) > 0:
        success_idx = success_idx.squeeze()
        suc_seed = seed_img[success_idx.cpu()]
        suc_fool = fool_img[success_idx.cpu()]
        diff_imgs = suc_fool - suc_seed
        # result = {
        # 'seed_imgs': suc_seed,
        # 'seed_labels':pred_seed[success_idx].cpu(),
        # 'targets':targets[success_idx].cpu(),
        # 'fool_imgs': suc_fool,
        # 'fool_labels':pred_fool[success_idx].cpu(),
        # 'diff_imgs': diff_imgs}
        return suc_seed,pred_seed[success_idx].cpu(),targets[success_idx].cpu(),suc_fool,pred_fool[success_idx].cpu(),diff_imgs
    else:
        return [],[],[],[],[],[]

def evaluate_example(seed_img, fool_img, model, reconstruction=False):
    seed_var = Variable(seed_img).cuda()
    fool_var = Variable(fool_img).cuda()
    if reconstruction:
        scores_seed, rec_seed = model(seed_var)
        scores_fool, rec_fool = model(fool_var)
    else:
        scores_seed = model(seed_var)
        scores_fool = model(fool_var)
    
    results = {
        'pred_seed': scores_seed.data.cpu().max(1)[1][0],
        'pred_fool': scores_fool.data.cpu().max(1)[1][0]}
    if reconstruction:
        results['mse_seed'] = torch.sum((rec_seed - seed_var)**2).data.cpu()[0]
        results['mse_fool'] = torch.sum((rec_fool - fool_var)**2).data.cpu()[0]
        reconstructions = {
            'rec_seed': rec_seed.data.cpu(),
            'rec_fool': rec_fool.data.cpu()}
        return results, reconstructions
    else:
        return results

def collect_attacks(final_res, results):
    if len(results[0]) == 0:
        return final_res
    #need to unsqueeze
    if len(results[0]) == 1:
        try:
            results = (results[0].unsqueeze(0),
                results[1].unsqueeze(0),
                results[2].unsqueeze(0),
                results[3].unsqueeze(0),
                results[4].unsqueeze(0),
                results[5].unsqueeze(0))
        except:
            print("Error: result tuple?")
            import ipdb;ipdb.set_trace()
    if len(final_res.keys()) == 0:
        final_res = {
        'seed_imgs': results[0],
        'seed_labels':results[1],
        'targets':results[2],
        'fool_imgs': results[3],
        'fool_labels':results[4],
        'diff_imgs': results[5]}
    else:
        try:
            final_res['seed_imgs'] = torch.cat([final_res['seed_imgs'], results[0]])
            final_res['seed_labels'] = torch.cat([final_res['seed_labels'], results[1]])
            final_res['targets'] = torch.cat([final_res['targets'], results[2]])
            final_res['fool_imgs'] = torch.cat([final_res['fool_imgs'], results[3]])
            final_res['fool_labels'] = torch.cat([final_res['fool_labels'], results[4]])
            final_res['diff_imgs'] = torch.cat([final_res['diff_imgs'], results[5]])
        except:
            import ipdb;ipdb.set_trace()
    return final_res

def measure_robustness(model, loader, attack_batch_N, attack_method = 'FGS', attack_args = {}):
    final_res = {}
    for i in range(attack_batch_N):
        print("attack batch {} / {}".format(i, attack_batch_N))
        batch, labels = next(iter(loader))
        batch, labels = remove_misclassified(batch, labels, model)
        # not possible to pack attack_method into attack_args (if want to pass into __init__, for attack_batch it's possible to get the keywords)
        adversary = Adversary(model, attack_method, **attack_args)
        fool_img = adversary.attack_batch(trycuda(batch), labels)
        result = keep_successful_attack(batch, fool_img, model) #, targeted = parse_targeted(attack_args))
        final_res = collect_attacks(final_res, result)
    return final_res